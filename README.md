# nrc_resources_public
This repository contains module install scripts of software packages needed to run the nrc_pipeline (https://bitbucket.org/jtremblay514/nrc_pipeline_public/src/master/)

For version 1.3.0 : https://bitbucket.org/jtremblay514/nrc_pipeline_public/src/1.3.0/
For version 1.3.1 (available soon) : https://bitbucket.org/jtremblay514/nrc_pipeline_public/src/1.3.1/

