module load nrc/conda/2
conda create -n deblurenv python=3.5 numpy
source activate deblurenv
conda install -c bioconda -c biocore VSEARCH MAFFT=7.310 biom-format SortMeRNA==2.0 deblur
