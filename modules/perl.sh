#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Perl
#

SOFTWARE=perl   
VERSION=5.26.0 
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget http://www.cpan.org/src/5.0/$SOFTWARE-$VERSION.tar.gz     
tar -xvf $SOFTWARE-$VERSION.tar.gz                             
cd $SOFTWARE-$VERSION                                         
## See post on stack overflow: https://stackoverflow.com/questions/45377100/error-generate-uudmap-o-during-perl-5-26-0-installation/45379233#45379233
#./Configure -des -Dusethreads -Dprefix=$INSTALL_PATH/$SOFTWARE-$VERSION -Dcc=gcc -Accflags="-std=c99 -I/cvmfs/soft.computecanada.ca/nix/var/nix/profiles/16.09/include"
./Configure -des -Dusethreads -Dprefix=$INSTALL_PATH/$SOFTWARE-$VERSION -Dcc=gcc -Accflags="-std=c99"
make  
make install

# Add permissions and install software
cd $INSTALL_DOWNLOAD
chmod -R ug+rwX .
chmod -R o+rX .
#mv -i $SOFTWARE-$VERSION.tar.gz $INSTALL_HOME/archive 

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;  
}
module-whatis \"$SOFTWARE-$VERSION  \" ;  
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION
prepend-path    PATH                \$root/bin                                                           
prepend-path    PERL5LIB            \$root/lib
prepend-path    PERL5LIB            \$root/lib/site_perl/5.26.0
prepend-path    PERL5LIB            \$root/lib/5.26.0
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD


