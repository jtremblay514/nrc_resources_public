#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name  muscle.
#

SOFTWARE=muscle
VERSION=3.8.31
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
mkdir -p $INSTALL_PATH/$SOFTWARE-$VERSION/bin
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget http://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_i86linux64.tar.gz
tar -xvf muscle3.8.31_i86linux64.tar.gz
cp muscle3.8.31_i86linux64 $INSTALL_PATH/$SOFTWARE-$VERSION/bin/
cd $INSTALL_PATH/$SOFTWARE-$VERSION/bin/
ln -s ./muscle3.8.31_i86linux64 ./muscle


# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ;

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
