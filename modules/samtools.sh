#!/bin/sh

#
# SAMtools
#
module load gcc/4.8.5
SOFTWARE=samtools
VERSION=1.9
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
#wget http://sourceforge.net/projects/$SOFTWARE/files/$SOFTWARE/$VERSION/$SOFTWARE-$VERSION.tar.bz2
wget http://sourceforge.net/projects/samtools/files/samtools/1.9/samtools-1.9.tar.bz2
tar jxvf $SOFTWARE-$VERSION.tar.bz2
cd $SOFTWARE-$VERSION
sed -i.bak 's/LIBCURSES\=  -lcurses/LIBCURSES\=  -lncurses/g' Makefile
make

# Add permissions and install software
cd $INSTALL_DOWNLOAD
chmod -R ug+rwX .
chmod -R o+rX .
mv -f $SOFTWARE-$VERSION $INSTALL_PATH/

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE \"
}
module-whatis \"$SOFTWARE SAM/BAM manipulation \"

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION
prepend-path    PATH                \$root
prepend-path    PATH                \$root/bcftools
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
