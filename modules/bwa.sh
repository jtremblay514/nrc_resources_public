module load gcc/4.8.5

###################
################### BWA
###################
VERSION="0.7.17"
INSTALL_PATH=$INSTALL_HOME/software/bwa/
mkdir -p $INSTALL_PATH
cd $INSTALL_PATH

# Download
wget https://github.com/lh3/bwa/releases/download/v$VERSION/bwa-$VERSION.tar.bz2
#wget http://downloads.sourceforge.net/project/bio-bwa/bwa-$VERSION.tar.bz2
tar xvjf bwa-$VERSION.tar.bz2

# Compile
cd bwa-$VERSION
sed -e "s#INCLUDES=#INCLUDES=-I $INSTALL_HOME/software/zlib/zlib-1.2.8/ #"  -e "s#-lz#$INSTALL_HOME/software/zlib/zlib-1.2.8/lib/libz.a#" Makefile > Makefile.zlib
make -f Makefile.zlib -j8
cd ..
chmod -R g+w $INSTALL_PATH/bwa-$VERSION

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - BWA \"
}
module-whatis \"NRC - BWA  \"
            
set             root               \$::env(INSTALL_HOME)/software/bwa/bwa-$VERSION
prepend-path    PATH               \$root
" > $VERSION
chmod 770 $VERSION

# Version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"
" > .version

mkdir -p $INSTALL_HOME/modulefiles/nrc/bwa
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/bwa/
rm bwa-$VERSION.tar.bz2

