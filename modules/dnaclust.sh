#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################

## Prepare
###################
################### dnaclust parallel release 3.
###################
## Install dnaclust itself
VERSION="3"
NAME=dnaclust
INSTALL_PATH=$INSTALL_HOME/software/$NAME/$NAME-$VERSION # where to install.
mkdir -p $INSTALL_HOME/software/$NAME
mkdir -p $INSTALL_PATH
mkdir -p $INSTALL_PATH/bin
wget http://sourceforge.net/projects/dnaclust/files/parallel_release_$VERSION/dnaclust_linux_release$VERSION.zip/download
mv download dnaclust_linux_release$VERSION.zip
unzip dnaclust_linux_release$VERSION.zip
cd ${NAME}_linux_release$VERSION
cp * $INSTALL_PATH/bin/

# Module def file..
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - Adds $NAME-$VERSION to your environment \"
}
module-whatis \"NRC - Adds $NAME-$VERSION to your environment \"
                       
set             root               \$::env(INSTALL_HOME)/software/$NAME/$NAME-$VERSION
prepend-path    PATH               \$root/bin

" > $VERSION

## THEN--> Move module definition manually, and edit .version

# version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"

" > .version

mkdir -p  $INSTALL_HOME/modulefiles/nrc/$NAME
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/$NAME

