#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name   GotoBLAS+LAPACK 
#
module load nrc/cmake/3.2.0
SOFTWARE=GotoBLAS+LAPACK 
VERSION=1.13
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
mkdir -p $INSTALL_PATH/$SOFTWARE-$VERSION
cd $INSTALL_DOWNLOAD

wget http://github.com/xianyi/OpenBLAS/archive/v0.2.14.tar.gz
tar -xvf v0.2.14.tar.gz
cd OpenBLAS-0.2.14
make clean
make BINARY=64 USE_THREAD=1 CC=gcc FC=gfortran TARGET=NEHALEM NUM_THREADS=132
make install PREFIX=$INSTALL_PATH/$SOFTWARE-$VERSION
cd $INSTALL_PATH/$SOFTWARE-$VERSION/lib
ln -s libopenblas_nehalemp-r0.2.14.so libblas.so
ln -s libopenblas_nehalemp-r0.2.14.so libcblas.so

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
module load nrc/gcc/4.8.2
cd $INSTALL_DOWNLOAD
wget http://www.netlib.org/lapack/lapack-3.5.0.tgz
tar -xvf lapack-3.5.0.tgz
cd lapack-3.5.0
cp make.inc.example make.inc
sed -i -e 's/BLASLIB      = \.\.\/\.\.\/librefblas.a/BLASLIB      = \$INSTALL_PATH\/\$SOFTWARE-\$VERSION\/lib\/libblas\.so/g' make.inc
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$SCRATCH/lapack-3.5.0 -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=ON ../
make
cp -f lib/liblapack.so $INSTALL_PATH/$SOFTWARE-$VERSION/lib
cp -f lib/libtmglib.so $INSTALL_PATH/$SOFTWARE-$VERSION/lib

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ;

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    LD_LIBRARY_PATH     \$root/lib
setenv          BLASDIR             \$root

" > $VERSION
#/software/CentOS-6/libraries/GotoBLAS_LAPACK

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
