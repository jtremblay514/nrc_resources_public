#module load gcc/4.8.5
###################
################### HMMER
###################
VERSION="3.3.2"
INSTALL_PATH=$INSTALL_HOME/software/hmmer/hmmer-$VERSION
INSTALL_DOWNLOAD=$INSTALL_HOME/software/hmmer/tmp
mkdir -p $INSTALL_PATH $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Adjust remote download URL according to version first number
if [[ ${VERSION:0:1} == 3 ]]
then
  SUFFIX=3
else
  SUFFIX=""
fi

# Download and extract
#wget http://selab.janelia.org/software/hmmer$SUFFIX/$VERSION/hmmer-$VERSION.tar.gz 
wget http://eddylab.org/software/hmmer/hmmer-3.3.2.tar.gz
tar -xvf hmmer-$VERSION.tar.gz

# Compile and install
cd hmmer-$VERSION
./configure --prefix $INSTALL_PATH
make
make check
make install
cd ..
chmod -R g+w $INSTALL_PATH

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - hmmer-$VERSION \"
}
module-whatis \"NRC - hmmer-$VERSION \"

set             root               \$::env(INSTALL_HOME)/software/hmmer/hmmer-$VERSION/bin
prepend-path    PATH               \$root
" > $VERSION

# Version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"
" > .version

mkdir -p $INSTALL_HOME/modulefiles/nrc/hmmer
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/hmmer/
rm -rf $INSTALL_DOWNLOAD
