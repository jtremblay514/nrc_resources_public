#!/bin/sh

#
# BLAST
#


SOFTWARE=blast
VERSION=2.13.0+
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract
wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.13.0+-x64-linux.tar.gz
tar zxvf ncbi-$SOFTWARE-$VERSION-x64-linux.tar.gz

# Add permissions and install software
cd $INSTALL_DOWNLOAD
chmod -R ug+rwX .
chmod -R o+rX .
mv -i ncbi-$SOFTWARE-$VERSION $INSTALL_PATH

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - NCBI $SOFTWARE \" ;
}
module-whatis \"NRC NCBI Blast: blast alignment \" ;
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/ncbi-$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;
prepend-path    BLASTDB             \$::env(INSTALL_HOME)/databases/ncbi_nr ;
prepend-path    BLASTDB             \$::env(INSTALL_HOME)/databases/ncbi_nt ;
prepend-path    BLASTDB             \$::env(INSTALL_HOME)/genomes/blast_db ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
