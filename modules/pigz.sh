#!/bin/bash


###################
################### pigz
###################
SOFTWARE=pigz
VERSION="2.3.4"
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE/
mkdir -p $INSTALL_PATH
mkdir -p $INSTALL_PATH/$SOFTWARE/$SOFTWARE-$VERSION/bin
cd $INSTALL_PATH

module load gcc/4.8.5

# Download
wget https://zlib.net/pigz/pigz-2.3.4.tar.gz
    # http://zlib.net/pigz/pigz-2.3.1.tar.gz
tar xvf pigz-${VERSION}.tar.gz
# Compile
cd pigz-${VERSION}
make

cp pigz $INSTALL_PATH/$SOFTWARE/$SOFTWARE-$VERSION/bin
cp unpigz $INSTALL_PATH/$SOFTWARE/$SOFTWARE-$VERSION/bin

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - pigz \"
}
module-whatis \"A parallel implementation of gzip for modern multi-processor, multi-core machines\"
            
set             root               \$::env(INSTALL_HOME)/software/pigz/pigz-${VERSION}
prepend-path    PATH               \$root/bin
" > $VERSION

# version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"
" > .version

mkdir -p $INSTALL_HOME/modulefiles/nrc/pigz
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/pigz/


