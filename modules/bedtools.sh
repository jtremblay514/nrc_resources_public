module load gcc/4.8.5
###################
################### BEDTools
###################
VERSION="2.17.0"
INSTALL_PATH=$INSTALL_HOME/software/bedtools
INSTALL_DOWNLOAD=$INSTALL_HOME/software/bedtools/tmp

mkdir -p $INSTALL_PATH $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download and extract
#wget "http://bedtools.googlecode.com/files/BEDTools.v$VERSION.tar.gz"
wget http://pkgs.fedoraproject.org/repo/pkgs/BEDTools/BEDTools.v2.17.0.tar.gz/e7209a6f88f8df844474369bd40db8be/BEDTools.v2.17.0.tar.gz
tar -xvf BEDTools.v$VERSION.tar.gz

# Change the program directory name since different tar.gz archive versions have different names
mv *-$VERSION $INSTALL_PATH/bedtools-$VERSION
cd $INSTALL_PATH/bedtools-$VERSION
make -j8
cd ..
chmod -R g+w $INSTALL_PATH/bedtools-$VERSION

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - BEDtools \"
}
module-whatis \"NRC - BEDtools  \"
                      
set             root               \$::env(INSTALL_HOME)/software/bedtools/bedtools-$VERSION/bin
prepend-path    PATH               \$root
" > $VERSION

# Version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"
" > .version

mkdir -p $INSTALL_HOME/modulefiles/nrc/bedtools
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/bedtools/
rm -rf $INSTALL_DOWNLOAD
