#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name hdf5.
#
SOFTWARE=hdf5
VERSION=1.8.13
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
#wget ftp://ftp.hdfgroup.org/HDF5/current/src/hdf5-1.8.13.tar.gz
wget --no-check-certificate http://ftp.mcs.anl.gov/pub/petsc/externalpackages/hdf5-1.8.13.tar.gz
tar -xvf $SOFTWARE-$VERSION.tar.gz  
cd $SOFTWARE-$VERSION                                                           
./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION                            
make                                                                             
make install

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ; 
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;
prepend-path    LD_LIBRARY_PATH     \$root/lib ;   
prepend-path    LD_LIBRARY_PATH     \$root/include ;   
prepend-path    LD_LIBRARY_PATH     \$root ;
prepend-path    HDF5_DIR            \$root ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
