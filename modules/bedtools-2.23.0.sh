module load gcc/4.8.5
###################
################### BEDTools
###################
VERSION="2.23.0"
INSTALL_PATH=$INSTALL_HOME/software/bedtools
INSTALL_DOWNLOAD=$INSTALL_HOME/software/bedtools/tmp
SOFTWARE=bedtools
mkdir -p $INSTALL_PATH $INSTALL_DOWNLOAD
mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin
cd $INSTALL_DOWNLOAD

# Download and extract
#wget "http://bedtools.googlecode.com/files/BEDTools.v$VERSION.tar.gz"
wget https://github.com/arq5x/bedtools2/archive/v2.23.0.tar.gz
tar -xvf v$VERSION.tar.gz

# Change the program directory name since different tar.gz archive versions have different names
#mv *-$VERSION $INSTALL_PATH/bedtools-$VERSION
cd bedtools2-$VERSION
make -j8
cd bin
cp ./* $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin/


# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - BEDtools \"
}
module-whatis \"NRC - BEDtools  \"
                      
set             root               \$::env(INSTALL_HOME)/software/bedtools/bedtools-$VERSION
prepend-path    PATH               \$root/bin
" > $VERSION

# Version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"
" > .version

mkdir -p $INSTALL_HOME/modulefiles/nrc/bedtools
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/bedtools/
#rm -rf $INSTALL_DOWNLOAD
