#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################

## Prepare
###################
################### diamond
###################
VERSION="3.15.0"
NAME=SPAdes
INSTALL_PATH=$INSTALL_HOME/software/$NAME/$NAME-$VERSION # where to install.
mkdir -p $INSTALL_HOME/software/$NAME
mkdir -p $INSTALL_PATH
mkdir -p $INSTALL_PATH/bin
wget http://cab.spbu.ru/files/release3.15.0/SPAdes-3.15.0-Linux.tar.gz
tar -xvf SPAdes-3.15.0-Linux.tar.gz
cd SPAdes-3.15.0-Linux
cp -r * $INSTALL_PATH/

# Module def file..
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - Adds $NAME-$VERSION to your environment \"
}
module-whatis \"NRC - Adds $NAME-$VERSION to your environment \"
                       
set             root               \$::env(INSTALL_HOME)/software/$NAME/$NAME-$VERSION
prepend-path    PATH               \$root/bin

" > $VERSION

## THEN--> Move module definition manually, and edit .version

# version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"

" > .version

mkdir -p  $INSTALL_HOME/modulefiles/nrc/$NAME
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/$NAME

