#!/bin/sh

#
# BLAST
#


SOFTWARE=blast
VERSION=2.12.1
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD
mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin

# Download, extract
wget https://bitbucket.org/berkeleylab/metabat/downloads/metabat-static-binary-linux-x64_v2.12.1.tar.gz
tar -xvf metabat-static-binary-linux-x64_v2.12.1.tar.gz
cd metabat

# Add permissions and install software
cd $INSTALL_DOWNLOAD
chmod -R ug+rwX .
chmod -R o+rX .
cp -r ./* $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin/

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - NCBI $SOFTWARE \" ;
}
module-whatis \"NRC MetaBAT2 \" ;
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
