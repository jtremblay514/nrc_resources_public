#!/bin/bash

#
# Trimmomatic
#

SOFTWARE=trimmomatic
VERSION=0.39
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-$VERSION.zip
unzip Trimmomatic-$VERSION.zip

# Add permissions and install software
cd $INSTALL_DOWNLOAD
chmod -R ug+rwX .
chmod -R o+rX .
mv -v -f Trimmomatic-$VERSION $INSTALL_PATH
#mv  -v-f Trimmomatic-$VERSION.zip $INSTALL_HOME/archive

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - Trimmomatic to trim fastq \" ;
}
module-whatis \"Trimmomatic to trim fastq  \" ;
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/Trimmomatic-$VERSION ;
setenv          TRIMMOMATIC_JAR     \$root/$SOFTWARE-$VERSION.jar ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
