#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name CAT
#

SOFTWARE=CAT
VERSION=5.2.3
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

mkdir -p $INSTALL_PATH/$SOFTWARE-$VERSION

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget https://github.com/dutilh/CAT/archive/v5.2.3.tar.gz
tar -xvf v5.2.3.tar.gz
cp -r CAT-5.2.3/* $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/

# Add permissions and install software
#cd $INSTALL_PATH
chmod -R 775 *

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ; 
   }
   module-whatis \"$SOFTWARE-$VERSION  \" ;  
                         
   set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
   prepend-path    PATH                \$root/CAT_pack ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
