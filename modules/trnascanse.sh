#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################

# Note: Install infernal 1.1.3 before installing trnascan. See symlinks generation below after actual installation.
#
# Software_name  trnascanse.
#
SOFTWARE=trnascan-se
VERSION=2.0.7
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget http://trna.ucsc.edu/software/trnascan-se-2.0.7.tar.gz
tar -xvf trnascan-se-2.0.7.tar.gz
cd tRNAscan-SE-2.0
./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION
make
make install
cd $INSTALL_PATH/$SOFTWARE-$VERSION/bin
ln -s ../../../infernal/infernal-1.1.3/bin/cmscan
ln -s ../../../infernal/infernal-1.1.3/bin/cmsearch

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ;

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
