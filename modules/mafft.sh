#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name  mafft.
#
SOFTWARE=mafft
VERSION=7.471
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin
mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/binaries
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget https://mafft.cbrc.jp/alignment/software/mafft-7.471-without-extensions-src.tgz
tar -xvf mafft-7.471-without-extensions-src.tgz
cd mafft-7.471-without-extensions/core
make
chmod 775 mafft
cp mafft $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin/
cd ..
cd binaries
cp * $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/binaries/

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ;

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;
prepend-path    MAFFT_BINARIES      \$root/binaries ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
