###################
################### BEDTools
###################
VERSION="2.29.2"
INSTALL_PATH=$INSTALL_HOME/software/bedtools
INSTALL_DOWNLOAD=$INSTALL_HOME/software/bedtools/tmp

mkdir -p $INSTALL_PATH $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download and extract
#wget "http://bedtools.googlecode.com/files/BEDTools.v$VERSION.tar.gz"
mkdir -p $INSTALL_HOME/software/bedtools/bedtools-2.29.2/bin
wget https://github.com/arq5x/bedtools2/releases/download/v2.29.2/bedtools.static.binary
cp bedtools.static.binary $INSTALL_HOME/software/bedtools/bedtools-2.29.2/bin

chmod -R g+w $INSTALL_PATH/bedtools-$VERSION

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - BEDtools \"
}
module-whatis \"NRC - BEDtools  \"
                      
set             root               \$::env(INSTALL_HOME)/software/bedtools/bedtools-$VERSION
prepend-path    PATH               \$root/bin
" > $VERSION

# Version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"
" > .version

mkdir -p $INSTALL_HOME/modulefiles/nrc/bedtools
mv .version $VERSION $INSTALL_HOME/modulefiles/nrc/bedtools/
rm -rf $INSTALL_DOWNLOAD
