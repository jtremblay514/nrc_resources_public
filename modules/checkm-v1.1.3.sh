#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name  checkm.
#
module load nrc/pplacer/1.1 nrc/prodigal/2.6.3 nrc/python/3.6.5 nrc/hmmer/3.1b2
SOFTWARE=checkm
VERSION=1.1.3
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION
cd $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION

# DO SEMI MANUALLY
python3 -m venv venv_checkm
source venv_checkm/bin/activate

export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/lib/python3.6/site-packages
#:$PYTHONPATH

CURR_NAME=numpy
CURR_VERSION=1.15.0
#pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 
pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 

CURR_NAME=matplotlib
CURR_VERSION=2.1.0
pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION

CURR_NAME=scipy
#CURR_VERSION=0.19.1
#pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION
pip install "$CURR_NAME" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION

CURR_NAME=pysam
#CURR_VERSION=0.12.0.1
#pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 
pip install "$CURR_NAME" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 

CURR_NAME=dendropy
#CURR_VERSION=4.4.0
#pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 
pip install "$CURR_NAME" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 

CURR_NAME=checkm-genome
#CURR_VERSION=1.1.3
#pip install "$CURR_NAME==$CURR_VERSION" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 
pip install "$CURR_NAME" --build $TMPDIR --prefix=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION 


# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget https://github.com/Ecogenomics/CheckM/archive/v1.1.3.tar.gz
tar -xvf v$VERSION.tar.gz
cd CheckM-1.1.3
python setup.py install --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION/

# With python 3.6:
#cd $INSTALL_HOME/checkm/checkm-1.1.3/lib/python3.6/site-packages
#mv checkm_genome-1.1.3.dist-info checkm_genome-1.1.3.dist-info.backup

# Add permissions and install software
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ;

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    CHECKM_HOME         \$root ;
prepend-path    PYTHONPATH          \$root/lib/python3.6/site-packages ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
