#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name R
#
SOFTWARE=R
BASE=4
VERSION=4.1.2 
MODULEFILE_DIR="$INSTALL_HOME/modulefiles/nrc/R"
MODULEFILE="$MODULEFILE_DIR/$VERSION"
mkdir -p ${MODULEFILE_DIR}
MODULEVERSIONFILE="$MODULEFILE_DIR/.version"

INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

wget https://cran.r-project.org/src/base/$SOFTWARE-$BASE/$SOFTWARE-$VERSION.tar.gz
tar -xvf $SOFTWARE-$VERSION.tar.gz
cd $SOFTWARE-$VERSION
./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION --enable-R-shlib # TEMP s--with-readline=yes --with-readline=no
make -j8
make install
cd $TMPDIR

# Create mechoodule files
cat > $MODULEFILE <<-EOF	
	#%Module1.0
	proc ModulesHelp { } {
	puts stderr "NRC - Adds R to your environment"
	}
	module-whatis "NRC - Adds R to your environment"	
	set             root               $INSTALL_PATH/$SOFTWARE-$VERSION
	setenv          R_LIBS             \$root/lib64/R/library
	#prepend-path   MANPATH            \$root/share              
	prepend-path    PATH               \$root/bin
	prepend-path    LD_LIBRARY_PATH    \$root/lib64/R/lib:/software/libraries/GotoBLAS_LAPACK/shared
	$GCC_MODULE_CALL
EOF
	
	## Define Rprofile.site: 
	# On headless nodes, can only use cairo backend OR have Xvb running
	# so we can have cairo-devel.x86_64 installed by admins, and then re-compile R. It should
	# then have cairo support. There is a weird behavior though. Accoring to the R-doc, cairo
	# is supposed to be set as the default backend -> getOption("bitmapType") on linux instead
	# of Xlib if cairo is available.
	# However it is not always the case. Not sure if this is a bug, might have something to do with pango present or not:
	# http://r.789695.n4.nabble.com/cairo-is-not-the-default-when-available-td4666691.html
	# http://r.789695.n4.nabble.com/Xll-options-td3725879.html
	#  so the only other way is to set options(bitmapType="cairo")
	# This can be set in R/lib64/etc/Rprofile.site, but then R should not be invked with R --vanilla
	# because vanilla will ignore Rprofile.site.


## Adjust permissions
chmod -R ug+rwX  $INSTALL_DIR $MODULEFILE $MODULEVERSIONFILE
chmod -R o+rX    $INSTALL_DIR $MODULEFILE $MODULEVERSIONFILE

# ## Test
# me="R.sh"
# export R_LIBS=
# VERSION="3.0.1"
# INSTALL_PREFIX_ENV_VARNAME="INSTALL_HOME"
# MODULEFILE_DIR="modulefiles/nrc_dev/R"
# INSTALL_DIR="software/R"
# #MODULEFILE_DIR="$INSTALL_HOME/modulefiles/nrc_dev/R"
# #INSTALL_DIR="$INSTALL_HOME/software/R"
# FORCE_INSTALL=true
# 

# ### Blurb to test graphics
# # module load nrc/R/3.0.2
# # R --no-save <<'EOF'
#  capabilities()
#  getOption("bitmapType") # returns default bitmap device: abacus=cairo,guil=cairo,mp2=Xlib
#  jpeg("temp.jpeg")#,type='cairo')
#  plot(1)
#  dev.off()
# # EOF

