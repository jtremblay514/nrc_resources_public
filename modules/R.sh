#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name R
#

SOFTWARE=R
BASE=3
VERSION=3.6.0 
MODULEFILE_DIR="$INSTALL_HOME/modulefiles/nrc/R"
MODULEFILE="$MODULEFILE_DIR/$VERSION"
mkdir -p ${MODULEFILE_DIR}
MODULEVERSIONFILE="$MODULEFILE_DIR/.version"

INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# On centos, do not forget to do the following before compiling R:
#yum install epel-release
#yum install yum-utils  # to make yum-builddep command available
#yum-builddep R

wget http://cran.r-project.org/src/base/$SOFTWARE-$BASE/$SOFTWARE-$VERSION.tar.gz
tar -xvf $SOFTWARE-$VERSION.tar.gz
cd $SOFTWARE-$VERSION
./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION  --with-readline=yes --with-x=yes
make -j8
make install
cd $TMPDIR

# Create mechoodule files
cat > $MODULEFILE <<-EOF	
	#%Module1.0
	proc ModulesHelp { } {
	puts stderr "NRC - Adds R to your environment"
	}
	module-whatis "NRC - Adds R to your environment"	
	set             root               $INSTALL_PATH/$SOFTWARE-$VERSION
	setenv          R_LIBS             \$root/lib64/R/library
	#prepend-path   MANPATH            \$root/share              
	prepend-path    PATH               \$root/bin
	prepend-path    LD_LIBRARY_PATH    \$root/lib64:/project/microbiome_genomics/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13/lib
EOF
cat > $MODULEVERSIONFILE <<-EOF
	#%Module1.0
	set ModulesVersion $VERSION
	EOF
	
## Adjust permissions
chmod -R ug+rwX  $INSTALL_DIR $MODULEFILE $MODULEVERSIONFILE
chmod -R o+rX    $INSTALL_DIR $MODULEFILE $MODULEVERSIONFILE

