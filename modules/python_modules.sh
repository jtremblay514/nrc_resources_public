#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name python-modules
#

module load nrc/openmpi/1.8.1-gcc
module load nrc/python/2.7.5

SOFTWARE=python_modules
VERSION=1.0
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/setuptools-39.1.0/lib/python2.7/site-packages/
wget https://pypi.python.org/packages/source/s/setuptools/setuptools-39.1.0.zip
unzip setuptools-39.1.0.zip
cd setuptools-39.1.0
export PYTHONPATH=$INSTALL_PATH/$SOFTWARE-$VERSION/setuptools-39.1.0/lib/python2.7/site-packages:$PYTHONPATH
python setup.py install --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION/setuptools-39.1.0

#mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/setuptools-17.1/lib/python2.7/site-packages/
#wget https://pypi.python.org/packages/source/s/setuptools/setuptools-17.1.tar.gz
#tar -xvf setuptools-17.1.tar.gz
#cd setuptools-17.1
#export PYTHONPATH=$INSTALL_PATH/$SOFTWARE-$VERSION/setuptools-17.1/lib/python2.7/site-packages:$PYTHONPATH
#python setup.py install --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION/setuptools-17.1

#mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/scons-2.4.0/lib/python2.7/site-packages/
#wget http://prdownloads.sourceforge.net/scons/scons-2.4.0.tar.gz
#tar -xvf scons-2.4.0.tar.gz
#cd scons-2.4.0
#export PYTHONPATH=$INSTALL_PATH/$SOFTWARE-$VERSION/scons-2.4.0/lib/python2.7/site-packages:$PYTHONPATH
#python setup.py install --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION/scons-2.4.0


# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ; 
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION;
prepend-path    PATH                \$root/setuptools-39.1.0/bin;
prepend-path    PYTHONPATH          \$root/setuptools-39.1.0/lib/python2.7/site-packages;
prepend-path    PATH                \$root/scons-2.4.0/bin;
prepend-path    PYTHONPATH          \$root/scons-2.4.0/lib/python2.7/site-packages;
" > $VERSION
################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
