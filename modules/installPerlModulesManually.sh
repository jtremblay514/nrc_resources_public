mkdir -p packages
cd packages

## Define install path here:
INSTALLPATH=$INSTALL_HOME/software/perl/perl-5.26.0

module load nrc/R/3.4.0

## Download modules here. Try to use <perl -MCPAN -e 'install This::Module'>. If it doesnt work, install it by hand.
#wget http://search.cpan.org/CPAN/authors/id/G/GR/GRANTM/XML-Simple-2.24.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/F/FA/FANGLY/Statistics-R-0.34.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/R/RO/ROBM/Cache-FastMmap-1.46.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/N/NW/NWCLARK/Devel-Size-0.80.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/P/PL/PLICEASE/File-Which-1.22.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/L/LE/LEONT/Module-Build-0.4224.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/M/MI/MIKEM/Net-SSLeay-1.81.tar.gz
#wget https://cpan.metacpan.org/authors/id/N/NA/NANIS/Crypt-SSLeay-0.73_06.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/S/SU/SULLR/IO-Socket-SSL-2.051.tar.gz
#wget http://search.cpan.org/CPAN/authors/id/G/GA/GAAS/LWP-Protocol-https-6.04.tar.gz
wget http://search.cpan.org/CPAN/authors/id/B/BI/BINGOS/ExtUtils-MakeMaker-7.30.tar.gz
wget http://search.cpan.org/CPAN/authors/id/R/RJ/RJBS/Carp-1.38.tar.gz
wget http://search.cpan.org/CPAN/authors/id/J/JK/JKEENAN/File-Path-2.14.tar.gz
wget http://search.cpan.org/CPAN/authors/id/R/RJ/RJBS/PathTools-3.62.tar.gz
wget http://search.cpan.org/CPAN/authors/id/R/RJ/RJBS/constant-1.33.tar.gz
wget http://search.cpan.org/CPAN/authors/id/S/SM/SMUELLER/AutoLoader-5.74.tar.gz
wget http://search.cpan.org/CPAN/authors/id/T/TO/TODDR/Exporter-5.72.tar.gz
## Define modules here in the MODULE array
MODULE=()


#MODULE[${#MODULE[@]}]=XML-Simple-2.24
#MODULE[${#MODULE[@]}]=Statistics-R-0.34
#MODULE[${#MODULE[@]}]=Cache-FastMmap-1.46
#MODULE[${#MODULE[@]}]=Devel-Size-0.80
#MODULE[${#MODULE[@]}]=File-Which-1.22
#MODULE[${#MODULE[@]}]=Module-Build-0.4224
#MODULE[${#MODULE[@]}]=Net-SSLeay-1.81
#MODULE[${#MODULE[@]}]=Crypt-SSLeay-0.73_06
#MODULE[${#MODULE[@]}]=IO-Socket-SSL-2.051
#MODULE[${#MODULE[@]}]=LWP-Protocol-https-6.04
MODULE[${#MODULE[@]}]=ExtUtils-MakeMaker-7.30
MODULE[${#MODULE[@]}]=Carp-1.38
MODULE[${#MODULE[@]}]=File-Path-2.14
MODULE[${#MODULE[@]}]=PathTools-3.62
MODULE[${#MODULE[@]}]=constant-1.33
MODULE[${#MODULE[@]}]=AutoLoader-5.74
MODULE[${#MODULE[@]}]=Exporter-5.72

## Loop the MODULE array and install modules.
for element in "${MODULE[@]}"; do

    echo "Installing module $element"
	tar -xvf $element.tar.gz
	cd $element
	perl Makefile.PL PREFIX=$INSTALLPATH
	make
	make install
	cd ..
 
done

cd ..

chmod -R 775 $INSTALL_HOME/software/perl
