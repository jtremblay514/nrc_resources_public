#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name nrc_pipeline
#

SOFTWARE=nrc_pipeline_public
VERSION=dev
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

mkdir -p $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
# tagged version
#git clone -b "$VERSION" https://jtremblay514@bitbucket.org/jtremblay514/nrc_pipeline_public.git
# dev/master version
git clone https://jtremblay514@bitbucket.org/jtremblay514/nrc_pipeline_public.git
cd $SOFTWARE
cp -r * $INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ;

set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PYTHONPATH          \$root
prepend-path    PATH                \$root/pipelines/illumina
prepend-path    PATH                \$root/pipelines/amplicontagger
prepend-path    PATH                \$root/pipelines/metagenomics
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
