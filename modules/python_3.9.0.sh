#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################

# If installing on RHEL, do not forget to install libffi.
#yum install libffi-devel

#
# Software_name  python.
#

SOFTWARE=python
VERSION=3.9.0
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
export PATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin:$PATH
export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/lib/python3.9/site-packages:$PYTHONPATH

#wget http://bzip.org/1.0.6/bzip2-1.0.6.tar.gz
wget https://sourceforge.net/projects/bzip2/files/bzip2-1.0.6.tar.gz
tar xpzf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make
make -f Makefile-libbz2_so
make install PREFIX=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bz2

export LD_LIBRARY_PATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bz2/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bz2/include:$LD_LIBRARY_PATH
export PATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bz2/bin:$PATH


wget https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tgz --no-check-certificate
tar -xvf Python-$VERSION.tgz 
cd Python-$VERSION                                                            
./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION --with-bz2           
make                                                                             
make install

export PATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/bin:$PATH
export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/lib:$PYTHONPATH
export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/lib/python3.9/site-packages:$PYTHONPATH

#pip3 install --upgrade pip setuptools wheel
#pip3 install "numpy==1.14.3"
#pip3 install "scipy>=0.13.0"
#pip3 install "scikit-bio>=0.4.0"
#pip3 install "biom-format"
#pip3 install "pandas"
      

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ; 
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION ;
prepend-path    PATH                \$root/bin ;  
prepend-path    PYTHONPATH          \$root/lib ;  
prepend-path    PYTHONPATH          \$root/lib/python3.9/site-packages ;  
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
