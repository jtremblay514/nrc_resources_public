#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name qiime-dependencies
#

#module load nrc/openmpi/1.8.1-gcc
module load nrc/python/2.7.5
module load nrc/hdf5/1.8.13
module load nrc/pip/10.0.1
module load nrc/python_modules/1.0
module load nrc/GotoBLAS+LAPACK/1.13
export MKLROOT=$INSTALL_HOME/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13/lib:$INSTALL_HOME/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13:$INSTALL_HOME/software/mkl
export BLAS=$INSTALL_HOME/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13/lib
export LAPACK=$INSTALL_HOME/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13/lib

SOFTWARE=qiime-dependencies
VERSION=1.9.1
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Uncomment lines of python modules that needs to be installed.

CURR_VERSION=1.9.0
CURR_NAME=numpy
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/numpy/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_VERSION=0.14.0
CURR_NAME=scipy
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=matplotlib
CURR_VERSION=1.1.0
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_VERSION=1.5.3
CURR_NAME=cogent
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/numpy/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_VERSION=2.3.0
CURR_NAME=pathlib2
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/numpy/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=cython
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=pynast
CURR_VERSION=1.2.2
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=qcli
CURR_VERSION=0.1.1
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=gdata
CURR_VERSION=
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=biom-format
CURR_VERSION=2.1.3
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=emperor
CURR_VERSION=0.9.5
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=scikit-bio
CURR_VERSION=0.2.2
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=burrito-fillings
CURR_VERSION=0.1.0
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=pandas
CURR_VERSION=0.19.2
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=ete2
CURR_VERSION=2.3.10
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=burrito
CURR_VERSION=0.9.1
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=qiime-default-reference
CURR_VERSION=0.1.1
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME==$CURR_VERSION" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=h5py
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=pysam
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=dendropy
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

CURR_NAME=ScreamingBackpack
#export PYTHONPATH=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME/lib/python:$PYTHONPATH
#pip install "$CURR_NAME" --install-option="--home=$INSTALL_HOME/software/$SOFTWARE/$SOFTWARE-$VERSION/$CURR_NAME"

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ; 
                      
set             root                \$::env(INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE-$VERSION;
prepend-path    PATH                \$root/scipy/bin ;
prepend-path    PATH                \$root/biom-format/bin ;
prepend-path    PATH                \$root/pynast/bin ;
prepend-path    PATH                \$root/qcli/bin ;
prepend-path    PATH                \$root/gdata/bin ;
prepend-path    PATH                \$root/emperor/bin ;
prepend-path    PATH                \$root/burrito/bin ;
prepend-path    PATH                \$root/burrito-fillings/bin ;
prepend-path    PATH                \$root/pandas/bin ;
prepend-path    PATH                \$root/numpy/bin ;
prepend-path    PATH                \$root/h5py/bin ;
prepend-path    PATH                \$root/cython/bin ;
prepend-path    PATH                \$root/mpi4py/bin ;
prepend-path    PATH                \$root/pysam/bin ;
prepend-path    PATH                \$root/dendropy/bin ;
prepend-path    PATH                \$root/ScreamingBackpack/bin ;
prepend-path    PATH                \$root/matplotlib/bin ;
prepend-path    PATH                \$root/ete2/bin ;
prepend-path    PATH                \$root/cogent/bin ;
prepend-path    PYTHONPATH          \$root/numpy/lib/python ;
prepend-path    PYTHONPATH          \$root/h5py/lib/python ;
prepend-path    PYTHONPATH          \$root/cython/lib/python ;
prepend-path    PYTHONPATH          \$root/mpi4py/lib/python ;
prepend-path    PYTHONPATH          \$root/pysam/lib/python ;
prepend-path    PYTHONPATH          \$root/dendropy/lib/python ;
prepend-path    PYTHONPATH          \$root/ScreamingBackpack/lib/python ;
prepend-path    PYTHONPATH          \$root/matplotlib/lib/python ;
prepend-path    PYTHONPATH          \$root/scipy/lib/python ;
prepend-path    PYTHONPATH          \$root/biom-format/lib/python ;
prepend-path    PYTHONPATH          \$root/pynast/lib/python ;
prepend-path    PYTHONPATH          \$root/qcli/lib/python ;
prepend-path    PYTHONPATH          \$root/gdata/lib/python ;
prepend-path    PYTHONPATH          \$root/emperor/lib/python ;
prepend-path    PYTHONPATH          \$root/burrito/lib/python ;
prepend-path    PYTHONPATH          \$root/burrito-fillings/lib/python ;
prepend-path    PYTHONPATH          \$root/pandas/lib/python ;
prepend-path    PYTHONPATH          \$root/ete2/lib/python ;
prepend-path    BLAS                $INSTALL_HOME/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13/lib
prepend-path    LAPACK              $INSTALL_HOME/software/GotoBLAS+LAPACK/GotoBLAS+LAPACK-1.13/lib
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE

# Clean up temporary installation files if any
#rm -rf $INSTALL_DOWNLOAD
