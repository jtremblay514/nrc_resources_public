#!/bin/sh

################################################################################
# This is a module install script template which should be copied and used for
# consistency between module paths, permissions, etc.
# Only lines marked as "## TO BE ADDED/MODIFIED" should be, indeed, modified.
# You should probably also delete this commented-out header and the ## comments
################################################################################


#
# Software_name openmpi.
#
# help on how to build: http://www.open-mpi.org/faq/?category=building#build-compilers

SOFTWARE=openmpi
VERSION=1.8.1
GCC_VERSION=4.8.2
INSTALL_PATH=$INSTALL_HOME/software/$SOFTWARE-gcc
INSTALL_DOWNLOAD=$INSTALL_PATH/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
wget http://www.open-mpi.org/software/ompi/v1.8/downloads/openmpi-$VERSION.tar.gz
tar -xvf $SOFTWARE-$VERSION.tar.gz  
cd $SOFTWARE-$VERSION

make distclean 
#./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION --with-psm-libdir=/usr/lib64 --enable-static --with-tm=/opt/torque
./configure --prefix=$INSTALL_PATH/$SOFTWARE-$VERSION --enable-static
make install

# Add permissions and install software
chmod -R 775 *
cd $INSTALL_DOWNLOAD

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tNRC - $SOFTWARE-$VERSION compiled with GCC $GCC_VERSION \" ;
}
module-whatis \"$SOFTWARE  \" ; 
                      
set             root                    \$::env(INSTALL_HOME)/software/$SOFTWARE-gcc/$SOFTWARE-$VERSION ;
prepend-path    PATH                    \$root/bin ;
prepend-path    LD_LIBRARY_PATH         \$root/lib:/opt/torque/x86_64/lib ;
prepend-path    LIBRARY_PATH            \$root/lib ;
setenv          OMP_NUM_THREADS         1
setenv          PSM_SHAREDCONTEXTS_MAX  6
" > $VERSION
#prepend-path    OMPI_MCA_mtl            ^psm

################################################################################
# Everything below this line should be generic and not modified

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Add permissions and install module
mkdir -p $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
chmod -R ug+rwX $VERSION .version
chmod -R o+rX $VERSION .version
mv $VERSION .version $INSTALL_HOME/modulefiles/nrc/$SOFTWARE
mv $INSTALL_HOME/modulefiles/nrc/$SOFTWARE/$VERSION $INSTALL_HOME/modulefiles/nrc/$SOFTWARE/$VERSION-gcc

# Clean up temporary installation files if any
rm -rf $INSTALL_DOWNLOAD
